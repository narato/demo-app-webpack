module.exports = {
    title: 'Demoapp app | ACPaaS Angular App generator',
    description: 'Demoapp app made with the ACPaaS Angular App generator.',
    baseUrl: '/',
    themeColor: '#CE1A32',
    GTMContainerId: '',
    GATag: ''
};
