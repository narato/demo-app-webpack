import {
    Component,
    ComponentFactoryResolver,
    ViewContainerRef,
    ViewChild
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import { LayoutRendererComponent } from 'abs-layout-renderer';

import { Dossier, DossierList, DossierListActionCreator } from '../../store/dossier-list';

@Component({
    selector: 'home-page',
    styleUrls: ['./home.page.scss'],
    templateUrl: './home.page.html'
})
export class HomePage {
    public moduleStore = 'http://localhost:3002/';
    public templateStore = 'http://localhost:3002/templates/';
    public templateId = 'my-template-1.html';
    public dossierNr: string;
    @select(['dossierList']) dossiers$: Observable<DossierList>;

    @ViewChild('lrContainer', { read: ViewContainerRef }) lrContainer: ViewContainerRef;

    constructor(private cmpFactoryResolver: ComponentFactoryResolver, private dossierListActions: DossierListActionCreator) {}

    public loadTemplate() {
        const cmpFactory = this.cmpFactoryResolver.resolveComponentFactory(LayoutRendererComponent);
        this.lrContainer.clear();
        const cmpRef = this.lrContainer.createComponent(cmpFactory);
        cmpRef.instance.moduleStore = this.moduleStore;
        cmpRef.instance.templateStore = this.templateStore;
        cmpRef.instance.templateId = this.templateId;
        cmpRef.instance.context = { dossierNr: this.dossierNr };
        cmpRef.instance.onEmitted.subscribe((event) => {
            // this.dossiers.push(event);
            this.dossierListActions.addDossier(event);
            console.log('Host app received an event from dynamic component: ', event)
            this.lrContainer.clear();
        });
        this.dossierNr = '';
    }

}
