import { combineReducers } from 'redux';
import { Counter, COUNTER_INITIAL_STATE, counterReducer } from './counter';
import { Dossier, DossierList, DOSSIER_LIST_INITIAL_STATE, dossierListReducer } from './dossier-list'

export interface AppState {
    counter: Counter;
    dossierList: DossierList;
};

export const rootReducer = combineReducers<AppState>({
    counter: counterReducer,
    dossierList: dossierListReducer
});

export const initialState: AppState = {
    counter: COUNTER_INITIAL_STATE,
    dossierList: DOSSIER_LIST_INITIAL_STATE
};

export const storeMiddleware = [
];

export * from './store.module';
