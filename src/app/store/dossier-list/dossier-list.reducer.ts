import { Dossier, DossierList, DossierListAction } from './dossier-list.types';
import { DOSSIER_LIST_INITIAL_STATE } from './dossier-list.initial-state';
import * as actionTypes from './dossier-list.action-types';

export const dossierListReducer = (
    state: DossierList = DOSSIER_LIST_INITIAL_STATE,
    action: DossierListAction
) => {
    switch (action.type) {
        case actionTypes.ADD_DOSSIERS:
            return [...action.payload];
        case actionTypes.ADD_DOSSIER:
            return [...state, action.payload]
        default:
            return state;
    }
};
