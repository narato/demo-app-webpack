export class Dossier {
  dossierNr: string;
  subject: string;
  description: string;
  type: string;
};

export type DossierList = Array<Dossier>;

export type DossierListAction = {
    type: string;
    payload: DossierList;
}
