import { NgModule } from '@angular/core';
import { NgReduxModule } from '@angular-redux/store';

import { CounterActionCreator } from './counter';
import { DossierListActionCreator } from './dossier-list';

@NgModule({
    imports: [
        NgReduxModule
    ],
    providers: [
        // list your injectable services (like action-creators) here
        CounterActionCreator,
        DossierListActionCreator
    ]
})
export class StoreModule {}
