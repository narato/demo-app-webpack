import { Counter } from './counter.types';

export const COUNTER_INITIAL_STATE = {
    previous: 0,
    value: 0
};
